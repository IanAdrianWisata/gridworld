#include "GridWorld.h"
#include <cctype>



GridWorld::GridWorld()
{
	cout << Greetings();
	setupGridWorld();
	fStatus = true;
	fInput = false;
	fHero =  Hero();
	fGold = Gold();
	fTraps[0] = Trap(1, 2);
	fTraps[1] = Trap(3, 5);
	fTraps[2] = Trap(5, 3);
	
	fGrid[fHero.getRow()][fHero.getCol()] = "S";
	fGrid[fGold.getRow()][fGold.getCol()] = "G";


	for (int i = 0; i < 3; i++)
	{
		fGrid[fTraps[i].getRow()][fTraps[i].getCol()] = "D";
	}
	DrawGridWorld();
	cout << getDirections();
}

void GridWorld::setupGridWorld()
{
	for (int i = 0; i < 8; i++)
	{
		fGrid[0][i] = "#";
		fGrid[i][0] = "#";
		fGrid[i][7] = "#";
		fGrid[7][i] = "#";
	}

	for (int i = 1; i < 7; i++)
	{
		for (int j = 1; j < 7; j++)
		{
			fGrid[i][j] = " ";
		}
	}

	//If it has reach column 8, put into a new line
	for (int i = 0; i < 8; i++)
	{
		if (fGrid[i][7] == "#")
		{
			fGrid[i][7] = "# \n";
		}
	}
	
	for (int i = 0; i < 3; i++)
	{
		fGrid[i+1][3] = "#";
	}
}

void GridWorld::UpdateGridWorld()
{
	if (fStatus)
	{
		cout << getDirections();
		if (fHero.getRow() == fGold.getRow() && fHero.getCol() == fGold.getCol())
		{
			cout << "Congratulation you won the game \n";
			fStatus = false;
		}

		for (int i = 0; i < 3; i++)
		{
			if (fHero.getRow() == fTraps[i].getRow() && fHero.getCol() == fTraps[i].getCol())
			{
				cout << "You triggered a trap, you are dead \n";
				fStatus = false;
			}
		}
	}
}

void GridWorld::DrawGridWorld()
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			cout << fGrid[i][j];
		}
	}
}



void GridWorld::HandleInput()
{
	if (!fInput)
	{
		char lInput = '\0';

		cin >> lInput;
		lInput = tolower(lInput);
		fInput = true;

		if (lInput == 'n')
		{
			if (fHero.getRow() != 0 && fHero.getRow() != 1)
			{
				fGrid[fHero.getRow()][fHero.getCol()] = " ";
				fHero.moveUp();
				fGrid[fHero.getRow()][fHero.getCol()] = "S";
				cout << "You walk to the north \n";

			}
			else
			{
				cout << "cannot go north \n";

			}

		}
		else if (lInput == 's')
		{
			if (fHero.getRow() != 6)
			{
				fGrid[fHero.getRow()][fHero.getCol()] = " ";
				fHero.moveDown();
				fGrid[fHero.getRow()][fHero.getCol()] = "S";
				cout << "You walk to the south \n";

			}
			else
				cout << "cannot go south \n";
		}
		else if (lInput == 'w')
		{
			if (fHero.getCol() != 1 && fHero.getRow() != 0)
			{
				fGrid[fHero.getRow()][fHero.getCol()] = " ";
				fHero.moveLeft();
				fGrid[fHero.getRow()][fHero.getCol()] = "S";
				cout << "You walk to the west \n";

			}
			else
				cout << "cannot go west \n";

		}

		else if (lInput == 'e')
		{
			if (fHero.getCol() != 6 && fHero.getRow() != 0)
			{
				fGrid[fHero.getRow()][fHero.getCol()] = " ";
				fHero.moveRight();
				fGrid[fHero.getRow()][fHero.getCol()] = "S";
				cout << "You walk to the east \n";

			}
			else
				cout << "cannot go east \n";
		}

		else if (tolower(lInput) == 'q')
		{
			cout << "Quit the game \n";
			fStatus = false;

		}
		else
		{
			fInput = false;
			cout << "Wrong Command \n";
		}
		
	}
	
}

bool GridWorld::getStatus()
{
	return fStatus;
}

bool GridWorld::getInput()
{
	return fInput;
}

void GridWorld::changeInput(bool aInput)
{
	fInput = aInput;
}




string GridWorld::getDirections()
{
	string result = "Possible moves ";

	if (fHero.getCol() != 1)
	{
		result += "North; ";
	}
	if (fHero.getCol() != 1)
	{
		result += "West; ";
	}
	if (fHero.getCol() != 6 && fHero.getRow() !=0 )
	{
		result += "East; ";
	}
	if (fHero.getRow() != 6)
	{
		result += "South; ";
	}
	
	result += "\n";
	
	return result;
}

string GridWorld::Greetings()
{
	string lResult;
	lResult = "Welcome to GridWorld : Quantised Excitement. Fate is waiting for You! \n";
	lResult += "Here's the list of commands that you can use: \n";
	lResult += "N : to move to the North \n";
	lResult += "E : to move to the East \n";
	lResult += "W : to move to the West \n";
	lResult += "S : to move to the South \n";
	return lResult;
}


GridWorld::~GridWorld()
{
}
