#pragma once

#include "GridWorld.h"
#include <thread>

using namespace std;

GridWorld gridWorld;

void call_from_thread()
{
	gridWorld.HandleInput();
}
int main()
{
	thread t1;
	bool threadIsOn = false;

	while (gridWorld.getStatus() == true)
	{
		if (gridWorld.getInput() == false)
		{
			if (!threadIsOn)
			{
				t1 = thread(call_from_thread);
				threadIsOn = true;
			}
		}
		gridWorld.UpdateGridWorld();
		gridWorld.DrawGridWorld();
		
		if (gridWorld.getInput() == true)
		{
			t1.join();
			threadIsOn = false;
			gridWorld.changeInput(false);
		}
	}

	
}
