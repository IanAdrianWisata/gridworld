#include "Hero.h"



Hero::Hero()
{

}


Hero::~Hero()
{
}

void Hero::moveUp()
{
	fRow -= 1;
}

void Hero::moveDown()
{
	fRow += 1;
}

void Hero::moveLeft()
{
	fCol -= 1;
}

void Hero::moveRight()
{
	fCol += 1;
}

int Hero::getRow()
{
	return fRow;
}

int Hero::getCol()
{
	return fCol;
}
