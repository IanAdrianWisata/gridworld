#pragma once
#include <iostream>
#include <string>
#include "GameObject.h"
#include "Hero.h"
#include "Gold.h"
#include "Trap.h"

using namespace std;

class GridWorld
{

private:
	string fGrid[8][8];
	Hero fHero;
	Gold fGold;
	Trap fTraps[3];
	bool fStatus;
	bool fInput;
	


public:
	GridWorld();
	void setupGridWorld();
	void UpdateGridWorld();
	void DrawGridWorld();
	void HandleInput();
	bool getStatus();
	bool getInput();
	void changeInput(bool aInput);
	string getDirections();
	string Greetings();
	
	~GridWorld();
};

