#pragma once
#include "GameObject.h"
class Gold : public GameObject
{
	int fRow;
	int fCol;
public:
	Gold();
	int getRow();
	int getCol();
	~Gold();
};

