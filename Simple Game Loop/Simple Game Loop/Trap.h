#pragma once
#include "GameObject.h"
class Trap : public GameObject
{
private:
	int fRow;
	int fCol;
public:
	Trap();
	Trap(int aRow, int aCol);
	int getRow();
	int getCol();
	~Trap();
};

