#pragma once
#include <string>
#include <iostream>
#include <stdlib.h> 
#include <time.h>

using namespace std;

class GameObject
{
private:
	int fCol;
	int fRow;
public:
	GameObject();
	int getRow();
	int getCol();
	~GameObject();
};

