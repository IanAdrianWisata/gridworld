#pragma once
#include "GameObject.h"
class Hero : public GameObject
{
private:
	int fRow = 0;
	int fCol = 1;
public:
	Hero();
	~Hero();
	void moveUp();
	void moveDown();
	void moveLeft();
	void moveRight();

	int getRow();
	int getCol();
};

